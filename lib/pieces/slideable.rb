# Sliding pieces (Bishop/Rook/Queen)

module Slideable
  HORIZONTAL_AND_VERTICAL_DIRS = [
    [-1, 0], [0, -1], [1, 0], [0, 1]
  ].freeze
  DIAGONAL_DIRS = [
    [-1, -1], [1, -1], [1, 1], [-1, 1]
  ].freeze

  def horizontal_and_vertical_dirs
    HORIZONTAL_AND_VERTICAL_DIRS
  end

  def diagonal_dirs
    DIAGONAL_DIRS
  end

  def moves 
    moves = []

    move_dirs.each do | dx, dy |
      moves.concat(grow_unblocked_moves_in_dir(dx, dy))
    end

    moves
  end

  private

  def move_dirs 
    # implemented in subclass
    raise NotImplementedError
  end

  def grow_unblocked_moves_in_dir(dx, dy)
    current_x, current_y = position
    moves = []
   
    (1..7).each do | time |
      next_pos = [current_x + dx * time, current_y + dy * time]

      if board.valid_pos?(next_pos)
        if board[next_pos].empty?
          moves << next_pos
        else
          # take the opponent's piece
          moves << next_pos if board[next_pos].color != color

          # shouldn't be able to move past a blocking piece
          break
        end
      end
    end

    moves
  end
end
