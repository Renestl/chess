require_relative 'pieces'

class Board
  attr_accessor :rows

  def initialize
    @sentinel = NullPiece.instance
  
    setup_board
  end

  def [](pos)
    row, col = pos
    @rows[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @rows[row][col] = val
  end

  def empty?(pos)
    self[pos].empty?
  end

  def move_piece(start_pos, end_pos)
    raise 'There is no piece at starting position.' if empty?(start_pos)
    # raise 'Invalid end position.' if self[end_pos].class == Piece
    
    piece = self[start_pos]

    if !piece.moves.include?(end_pos)
      raise "This is not a valid move for this piece."
    end

    move_piece!(start_pos, end_pos)
  end

  def move_piece!(start_pos, end_pos)
    # update piece position on board
    piece = self[start_pos]

    self[end_pos] = piece
    self[start_pos] = sentinel
    piece.position = end_pos

    nil
  end

  def valid_pos?(pos)
    pos.all? { |coord | coord.between?(0,7) }
  end

  def add_piece(piece, pos)
    self[pos] = piece
  end

  def checkmate?(color)
    # If the player is in check, and if none of the player's pieces have any #valid_moves
    return false unless in_check?(color)
    
    pieces.select { |piece| piece.color == color }.all? { |piece| piece.valid_moves.empty? }
  end

  def in_check?(color)
    king_pos = find_king(color).position
    pieces.any? do |piece|
      piece.color != color && piece.moves.include?(king_pos)
    end
  end

  def find_king(color)
    pieces.find{ |piece| piece.is_a?(::King) && piece.color == color}
  end

  def pieces
    @rows.flatten.reject(&:empty?)
  end

  def dup
  end

  private

  attr_reader :sentinel

  def setup_back_row(color)
    back_pieces = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]

    row = color == :white ? 7 : 0

    back_pieces.each_with_index do |royalty, idx|
      royalty.new(color, self, [row, idx])
    end
  end

  def setup_pawn_row(color)
    row = color == :white ? 6 : 1

    8.times { |time| Pawn.new(color, self, [row, time])}
  end

  def setup_board
    # starting_rows = [0, 1, 6, 7].freeze

    @rows = Array.new(8) { Array.new(8, sentinel) }

    [:white, :black].each do |color|
      setup_back_row(color)
      setup_pawn_row(color)
    end
  end
end
