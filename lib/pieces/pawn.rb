require_relative 'piece'

class Pawn < Piece
  def symbol
    "♟"
  end
  
  def moves
    forward_steps + side_attacks
  end

  private

  def at_start_row?
    color == :white ? position[0] == 6 : position[0] == 1
  end

  def forward_dir
    # returns 1 or -1
    color == :white ? -1 : 1
  end

  def forward_steps
    cur_x, cur_y = position
    moves = []

    one_step = [cur_x + forward_dir, cur_y]
    two_step = [cur_x + 2 * forward_dir, cur_y]

    moves << one_step if board[one_step].empty?
    moves << two_step if at_start_row? && board[two_step].empty?

    moves.select { |pos| board.valid_pos?(pos)}
  end

  def side_attacks
    cur_x, cur_y = position
    moves = []

    side_moves = [[cur_x + forward_dir, cur_y + 1], [cur_x + forward_dir, cur_y - 1]]

    side_moves.each do |side_pos|
      if board.valid_pos?(side_pos)
        if !board[side_pos].empty? && board[side_pos].color != color
          moves << side_pos
        end
      end
    end

    moves.select { |pos| board.valid_pos?(pos)}
  end
end
