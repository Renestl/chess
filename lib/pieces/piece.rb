class Piece
  attr_reader :board, :color
  attr_accessor :position

  def initialize(color, board, position)
    @color = color
    @board = board
    @position = position

    @board.add_piece(self, position)
  end

  def to_s
    " #{symbol.colorize(color)}  "
  end

  def empty?
    false
  end

  def valid_moves
    # return array of places that a Piece can move to
    []
  end

  def symbol
    # piece symbol
    raise NotImplementedError
  end

  private

  def move_into_check?(end_pos)
  end
end
