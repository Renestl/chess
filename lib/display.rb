require 'colorize'
require_relative 'board'
require_relative 'cursor'

class Display
	attr_reader :board, :cursor

	def initialize(board)
		@board = board
		@cursor = Cursor.new([0,0], board)
	end	

	def render
		puts ("a".."h").to_a.join("   ")
		grid = board.rows.map.with_index do |row, row_idx|
			row.map.with_index do |piece, col_idx|
				pos = [row_idx, col_idx]
				if pos == cursor.cursor_pos && cursor.selected
					piece.to_s.colorize({:background => :light_red})
				elsif pos == cursor.cursor_pos
					piece.to_s.colorize({:background => :light_green})
				elsif (row_idx+col_idx).odd?
					piece.to_s.colorize({:background => :blue})
				#  piece.to_s
				elsif (row_idx+col_idx).even?
					piece.to_s.colorize({:background => :light_blue})
				end
			end.join
		end.join("\n")

		puts grid
	end

end
