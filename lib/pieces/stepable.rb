# Stepping pieces (Knight/King)

module Stepable
  def moves
    moves = []

    move_diffs.each do | (dx, dy) |  
      current_x, current_y = position
      
      next_pos = [current_x + dx, current_y + dy]

      if board.valid_pos?(next_pos)
        if board[next_pos].empty?
          moves << next_pos
        
        # take the opponent's piece
        elsif board[next_pos].color != color
          moves << next_pos
        end
      end
    end

    moves
  end

  private

  def move_diffs
    # implemented in subclass
    raise NotImplementedError
  end
end
